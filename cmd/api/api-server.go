package main

import (
	"context"
	"os"
	"os/signal"

	"computer-track/pkg/api"
	"computer-track/pkg/database"
	"computer-track/pkg/logger"

	"github.com/ansrivas/fiberprometheus/v2"
	"github.com/goccy/go-json"
	"github.com/gofiber/fiber/v2"
)

func main() {
	var ctx, cancel = context.WithCancel(context.Background())

	logger.NewZeroLogger()

	fiberApp := fiber.New(fiber.Config{
		BodyLimit:   1 * 1024, //1k
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
	})

	api.Init(ctx, database.NewMySqlDb())
	api.SetupApiRouter(fiberApp)

	prometheus := fiberprometheus.New("computer-tracking-api")
	prometheus.RegisterAt(fiberApp, "/metrics")
	fiberApp.Use(prometheus.Middleware)

	go func() {
		// Run server.
		if err := fiberApp.Listen(":8181"); err != nil {
			logger.Log.Error().Err(err).Msg("")
		}
	}()

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt) // Catch OS signals.
	<-sigint
	
	api.Close()
	cancel() //cancel the context

	// Received an interrupt signal, shutdown.
	if err := fiberApp.Shutdown(); err != nil {
		// Error from closing listeners, or context timeout:
		logger.Log.Error().Err(err).Msg("")
	}
}
