#!/usr/bin/env bash

tag=$1

result="$(grype computer-track:$tag --scope all-layers)"

totalCritical="$(echo $result | grep -s Critical | wc -l)"
totalHigh="$(echo $result | grep -s High | wc -l)"
totalMedium="$(echo $result | grep -s Medium | wc -l)"

echo $result | grep -s Critical
echo "--------"
echo "Total Critical: $totalCritical"
echo "--------"

echo $result | grep -s High
echo "--------"
echo "Total High: $totalHigh"
echo "--------"

echo $result | grep -s Medium
echo "--------"
echo "Total Medium: $totalMedium"
echo "--------"
