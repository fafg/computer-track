package application

import (
	"strings"

	db "computer-track/pkg/database"
	"computer-track/pkg/errors"
	"computer-track/pkg/models"
)

func InsertOrUpdateComputer(computer models.Computer) error {
	if len(strings.TrimSpace(computer.ComputerName)) == 0 || 
	   len(strings.TrimSpace(computer.MacAddress)) == 0 || 
	   len(strings.TrimSpace(computer.IpAddress)) == 0 {
		return &errors.StatusError{
			Message:    "Bad Request: Computer Name, Mac Address and IP Address are required fields",
			StatusCode: errors.BadRequest,
		}
	}

	return db.UpsertComputer(computer)
}

func GetComputers() ([]models.Computer, error) {
	return db.GetComputers()
}

func GetComputersByEmployeeAbbreviation(employeeAbbreviation string) ([]models.Computer, error) {
	if len(strings.TrimSpace(employeeAbbreviation)) == 0 {
		return nil, nil
	}
	
	return db.GetComputersByEmployeeAbbreviation(employeeAbbreviation)
}