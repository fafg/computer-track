package errors

import "fmt"

// int mapping
const (
	BadRequest          = 400
	Forbidden           = 403
	NotFound            = 404
	InternalServerError = 500
	ServiceUnavailable  = 503
)

type StatusError struct {
	Message    string
	StatusCode int
}

func (e *StatusError) Error() string {
	return fmt.Sprintf("%d: %s", e.StatusCode, e.Message)
}
