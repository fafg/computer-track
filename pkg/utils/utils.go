package utils

import (
	"net/http"

	"computer-track/pkg/errors"

	"github.com/gofiber/fiber/v2"
)

func HandleError(c *fiber.Ctx, err error) error {
	switch e := err.(type) {
	case *errors.StatusError:
		return c.Status(e.StatusCode).JSON(fiber.Map{
			"data": fiber.Map{
				"error": e.Message,
			},
		})
	default:
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"data": fiber.Map{
				"error": "Internal Server Error",
			},
		})
	}
}
