package router

import (
	nv1 "computer-track/pkg/api/computer/v1"

	"github.com/gofiber/fiber/v2"
)

func AddComputerRoutesV1(fiberApp *fiber.App) {
	var gv1 = fiberApp.Group("v1")

	gv1.Get("/computer", nv1.Get)
	gv1.Post("/computer", nv1.Post)
}

func AddComputerApiHealthCheck(fiberApp *fiber.App) {
	fiberApp.Get("/computer/status", nv1.Status)
}
