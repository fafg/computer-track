package api

import (
	"context"
	"runtime"

	"computer-track/pkg/api/router"
	"computer-track/pkg/config"
	"computer-track/pkg/database"
	"computer-track/pkg/logger"
	"computer-track/pkg/notification"

	"github.com/gofiber/fiber/v2"
)

func Init(ctx context.Context, db database.Database) {
	config.LoadSettings()
	database.Init(config.GetConnString(), db)
	database.OpenConnection()
	notification.Init(ctx)
}

func SetupApiRouter(fiber *fiber.App) {
	router.AddComputerRoutesV1(fiber)
	router.AddComputerApiHealthCheck(fiber)
}

func Close() {
	database.CloseConnection()
	runtime.GC()

	logger.Log.Info().Msg("application has closed successfully")
}
