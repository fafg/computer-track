package v1

import (
	"computer-track/pkg/application"
	"computer-track/pkg/errors"
	"computer-track/pkg/models"
	"computer-track/pkg/utils"

	fiber "github.com/gofiber/fiber/v2"
)

func Get(c *fiber.Ctx) error {
	var computers, err = application.GetComputers()
	if err != nil {
		return utils.HandleError(c, err)
	}

	return c.Status(fiber.StatusOK).JSON(computers)
}

func Post(c *fiber.Ctx) error {
	var computer = models.Computer{}
	if err := c.BodyParser(&computer); err != nil {
		return utils.HandleError(c, &errors.StatusError{
			StatusCode: errors.BadRequest,
			Message:    "Bad Request: Invalid JSON",
		})
	}

	if err := application.InsertOrUpdateComputer(computer); err != nil {
		return utils.HandleError(c, err)
	}

	return c.Status(fiber.StatusOK).JSON(computer)
}

func Status(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{
		"message": "API - V1 - Status: OK",
	})
}
