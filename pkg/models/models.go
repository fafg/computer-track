package models

type Computer struct {
	MacAddress           string `json:"macAddress"`
	ComputerName         string `json:"Name"`
	IpAddress            string `json:"ipAddress"`
	EmployeeAbbreviation string `json:"employeeAbbreviation"`
	Description          string `json:"description"`
}

type Notification struct {
	Level                string `json:"level"`
	EmployeeAbbreviation string `json:"employeeAbbreviation"`
	Message              string `json:"message"`
}
