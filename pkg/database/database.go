package database

import (
	"computer-track/pkg/models"
)

type Database interface {
	Init(connStr string)
	OpenConnection()
	CloseConnection()

	UpsertComputer(models.Computer) error
	GetComputers() ([]models.Computer, error)
	GetComputersByEmployeeAbbreviation(employeeAbbreviation string) ([]models.Computer, error)
}

var dbImpl Database

func Init(connStr string, db Database) {
	dbImpl = db
	dbImpl.Init(connStr)
}

func OpenConnection() {
	dbImpl.OpenConnection()
}

func CloseConnection() {
	dbImpl.CloseConnection()
}

func UpsertComputer(computer models.Computer) error {
	return dbImpl.UpsertComputer(computer)
}

func GetComputers() ([]models.Computer, error) {
	return dbImpl.GetComputers()
}

func GetComputersByEmployeeAbbreviation(employeeAbbreviation string) ([]models.Computer, error) {
	return dbImpl.GetComputersByEmployeeAbbreviation(employeeAbbreviation)
}