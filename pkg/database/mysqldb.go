package database

import (
	"database/sql"
	"fmt"
	"runtime"

	"computer-track/pkg/errors"
	_ "computer-track/pkg/logger"
	"computer-track/pkg/models"
	"computer-track/pkg/notification"

	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
)

type MySqlDb struct {
	ConnString string
	client     *sql.DB
}

func NewMySqlDb() *MySqlDb {
	return &MySqlDb{}
}

func (db *MySqlDb) Init(connStr string) {
	db.ConnString = connStr
}

func (db *MySqlDb) OpenConnection() {
	// 2 means: the amount of threads should use per cpu
	// 0 means: use all cpus available. Default is runtime.NumCPU
	var maxOpenConns = 2 * runtime.GOMAXPROCS(1)

	mysql, err := sql.Open("mysql", db.ConnString)
	if err != nil {
		log.Error().Err(err).Msg("error on trying to create a new mysql application")
	} else {
		log.Info().Msg("Database connection has been established successfully.")
	}

	mysql.SetMaxOpenConns(maxOpenConns)
	mysql.SetMaxIdleConns(maxOpenConns)

	db.client = mysql
}

func (db *MySqlDb) CloseConnection() {
	if db.client.Ping() == nil {
		err := db.client.Close()
		if err != nil {
			log.Error().Err(err).Msgf("error on trying to close mysql connection: %v", err)
			return
		}
		log.Info().Msg("Database connection has been closed.")
	}
}

func (db *MySqlDb) UpsertComputer(computer models.Computer) error {
	result, err := db.client.Exec(
		`
    INSERT INTO computers 
		(mac_address, computer_name, ip_address, employee_abbreviation, description) 
	VALUES 
		(?,?,?,?,?)
	ON DUPLICATE KEY UPDATE
		computer_name = ?,
		ip_address = ?,
		employee_abbreviation = ?,
		description = ?;
	`, //insert
		computer.MacAddress, computer.ComputerName, computer.IpAddress, computer.EmployeeAbbreviation, computer.Description,
		//update
		computer.ComputerName, computer.IpAddress, computer.EmployeeAbbreviation, computer.Description)
	if err != nil {
		log.Error().Err(err).Msgf("InsertComputer - error on trying to prepare query: %v", err)
		return err
	}

	if err != nil {
		log.Error().Err(err).Msgf("error inserting data: %v", err)
	} else {
		var affected, _ = result.RowsAffected()
		log.Info().Msgf("lines affected: %v", affected)
	}

	checkComputerAssignment(computer)

	return err
}

func checkComputerAssignment(computer models.Computer) {
	var computers, err = GetComputersByEmployeeAbbreviation(computer.EmployeeAbbreviation)
	if err != nil {
		log.Error().Err(err).Msgf("error on trying to get computers by employee abbreviation: %v", err)
		return
	}

	if len(computers) >= 3 {
		var notif = models.Notification{
			Level:                "warning",
			EmployeeAbbreviation: computer.EmployeeAbbreviation,
			Message:              fmt.Sprintf("Employee with %v computers", len(computers)),
		}

		notification.NotificationChannel <- notif
	}
}

func (db *MySqlDb) GetComputers() ([]models.Computer, error) {
	var key, value = "Function", "database/GetComputers"
	var mac_address, computer_name, ip_address, employee_abbreviation, description string
	var query = "select * from computers"

	rows, err := db.client.Query(query)
	err = getError(err, key, value)
	if err != nil {
		return nil, err
	}

	defer closeRows(rows, key, value)
	var result = []models.Computer{}
	for rows.Next() {
		err = rows.Scan(&mac_address, &computer_name, &ip_address, &employee_abbreviation, &description)
		err = getError(err, key, value)
		if err != nil {
			return result, err
		}

		result = append(result, models.Computer{
			MacAddress:           mac_address,
			ComputerName:         computer_name,
			IpAddress:            ip_address,
			EmployeeAbbreviation: employee_abbreviation,
			Description:          description,
		})
	}

	return result, err
}

func (db *MySqlDb) GetComputersByEmployeeAbbreviation(employeeAbbreviation string) ([]models.Computer, error) {
	var key, value = "Function", "database/GetComputers"
	var mac_address, computer_name, ip_address, employee_abbreviation, description string
	var query = "select * from computers where employee_abbreviation = ?"

	rows, err := db.client.Query(query, employeeAbbreviation)
	err = getError(err, key, value)
	if err != nil {
		return nil, err
	}

	defer closeRows(rows, key, value)
	var result = []models.Computer{}
	for rows.Next() {
		err = rows.Scan(&mac_address, &computer_name, &ip_address, &employee_abbreviation, &description)
		err = getError(err, key, value)
		if err != nil {
			return result, err
		}

		result = append(result, models.Computer{
			MacAddress:           mac_address,
			ComputerName:         computer_name,
			IpAddress:            ip_address,
			EmployeeAbbreviation: employee_abbreviation,
			Description:          description,
		})
	}

	return result, err
}

func closeRows(rows *sql.Rows, key, value string) {
	err := rows.Close()
	if err != nil {
		log.Error().Err(err).Str(key, value).Msg("error on trying to close rows")
	}
}

func getError(err error, key, value string) error {
	if err == nil {
		return err
	} else {
		log.Error().Err(err).Str(key, value).Msg("error on executing query")
		if err.Error() == "sql: no rows in result set" {
			return &errors.StatusError{
				Message:    "Not found",
				StatusCode: errors.NotFound,
			}
		} else {
			return &errors.StatusError{
				Message:    "Internal server error",
				StatusCode: errors.InternalServerError,
			}
		}
	}
}
