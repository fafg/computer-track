package notification

import (
	"bytes"
	"context"
	"io"
	"net/http"

	"computer-track/pkg/config"
	"computer-track/pkg/models"

	"github.com/goccy/go-json"
	"github.com/rs/zerolog/log"
)

var NotificationChannel chan models.Notification

func Init(ctx context.Context) {
	NotificationChannel = make(chan models.Notification)
	go sendNotificationEventListener(ctx)
}

func sendNotificationEventListener(ctx context.Context) {
	for {
		select {
		case <-ctx.Done(): // Done returns a channel that was closed when work done on behalf of this context is canceled
			return
		default:
			SendNotification(<-NotificationChannel)
		}
	}
}

func SendNotification(notification models.Notification) {
	var jsonBytes, err = json.Marshal(notification)
	if err != nil {
		log.Error().Err(err).Msg("error while marshalling notification")
		return
	}

	req, err := http.NewRequest("POST", config.GetNotificationEndpoint(), bytes.NewBuffer(jsonBytes))
	if err != nil {
		log.Error().Err(err).Msg("error while creating request")
		return
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("error while executing request")
		return
	}
	defer resp.Body.Close()
	log.Info().Msgf("response Status: %v", resp.Status)

	body, _ := io.ReadAll(resp.Body)
	log.Debug().Msgf("response Body: %v", string(body))
	log.Info().Msgf("sent notification to employee: %v with message :%v", notification.EmployeeAbbreviation, notification.Message)
}
