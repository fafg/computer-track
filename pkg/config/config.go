package config

import "os"

var apiConfig *config

type config struct {
	dbConnString string // DB_CONN_STRING
	notificationEndpoint string // NOTIFICATION_ENDPOINT
}

func LoadSettings() {
	apiConfig = &config{
		dbConnString: os.Getenv("DB_CONN_STRING"),
		notificationEndpoint: os.Getenv("NOTIFICATION_ENDPOINT"),
	}
}

func GetConnString() string {
	return apiConfig.dbConnString
}

func GetNotificationEndpoint() string {
	return apiConfig.notificationEndpoint
}