# Computer Track

### How to build

```shell
✓ usage: make [target]

build-common                   - execute build common tasks clean and mod tidy
build-static-release           - build a static release linux elf(binary)
build                          - build a debug binary to the current platform (windows, linux or darwin(mac))
ci-lint-docker                 - runs golangci-lint with docker container
ci-lint                        - runs golangci-lint
docker-build                   - build docker image
docker-scan                    - Scan for known vulnerabilities
help                           - Show this help message
scan                           - execute static code analysis
sonar-scan                     - start sonar qube locally with docker (you will need docker installed in your machine)
sonar-stop                     - stop sonar qube docker container
test                           - execute go test command
up                             - start docker compose
```

### Environment variables

```shell
export DB_CONN_STRING="computer_track_user:1234@tcp(localhost:3600)/computer-track-db"
export NOTIFICATION_ENDPOINT="http://localhost:8080/api/notify"
```

### General Considerations

> The application is a simple CRUD, with a simple notification system.

It is not defined what expect in terms of load of the application. In a scenario where the application is used by a lot of users, it could be necessary to implement a cache system to improve the performance of the application. In case of heavy insertion of computers, it could be necessary to implement a queue/event driven to process the notifications.

### Implementation Considerations
The code is organized in a way that is easy to understand and maintain. The code is organized in layers, where each layer has a single responsibility. The layers are:
1. Api - responsible for the http layer, and the http routes.
2. Application - responsible for the business logic.
3. Database - responsible for the database layer.
4. Notification - responsible for the notification layer.
5. Models - responsible for the models.
   
> Endpoints:

1. GET - /v1/computer - list all computers
> GET List all computers
besides the list all computers, depending of database size, it could be necessary to implement pagination.
2. POST - /v1/computer - create a new computer
> Depeding how much requests are expected, it could be necessary to implement a event driven to communicate with a notification system.

### Notification System
> There are many ways of implement the notification, but we can end up in a synchronous or asynchronous way.
1. Synchronous: during the post request, after computer insertion we can perform a select query against the database to check the amount of computers register to a employee. In this solution you can find a bottleneck in the database if the api is used for a very high load. this also can be a problem if the notification system is down.
2. Local Asynchronous: we can use a local queue to store the notifications, and a worker to process the notifications. Working similarly as an outbox pattern.
3. Local Asynchronous without queue, this can cause a back pressure if notification api doesn't respond in time. **(My Implementation)**
4. Distributed Asynchronous: we can use a distributed queue/or event driven to store the notifications, and a worker to process the notifications. Working similarly as an outbox pattern. The notification system can collect and be responsible define when send the notifications.

### Extras
1. Dockerfile
2. Docker-Compose
3. Observability (Metrics(prometheus and Grafana) and Logging)
4. Database Script
5. Security Scan (static code analysis and vulnerability scan)
   1. GoSec and Golangci-lint
   2. SonarQube
   3. Grype - vulnerability scan for docker images

With this extras we can have a better observability of the application, and a better security in our local development.

### What could be added/improved
1. Unit and Integration tests
2. Tracing
3. Local database initialization script
4. Implement resilient calls against notification system. (since it is an external dependency, we can't guarantee that it will be always available)
5. implement a distributed queue to process the notifications.
6. implement a distributed cache to improve the performance of the application.
7. implement api gateway with circuit breaker to stop sending requests to the computer api in case of too many failures.
8. Release repository with deployment scripts.

### Database Considerations
> Storage optimization:

1. Some fields could use a different type, in order to save space.
- ip_address could be varbinary(4) for example.
- mac_address could be varbinary(6) for example.

2. index for abbreviation name could be created to optimize the search.

### Post Request Example
```shell
curl --request POST \
  --url http://localhost:8181/v1/computer \
  --header 'Content-Type: application/json' \
  --data '{
  "macAddress": "00:00:5e:00:53:af",
  "Name": "computer-1",
  "ipAddress": "192.168.10.20",
  "employeeAbbreviation": "mmu",
  "description": "work computer"
}'
```

Table structure for computer
```sql
CREATE TABLE `computers` (
     `mac_address` varchar(17) NOT NULL, --required
     `computer_name` varchar(255) NOT NULL, --required
     `ip_address` varchar(45) NOT NULL, --required
     `employee_abbreviation` varchar(3) DEFAULT NULL, --optional
     `description` varchar(255) DEFAULT NULL, --optional

     PRIMARY KEY (`mac_address`)
) 
```