import { check } from 'k6';
import http from 'k6/http';

const months = ["January", "February", "March", "April", "May", "June", "July",
"August", "September", "October", "November", "December"];

const date = new Date();

export let options = {
  tags: {
    testid: `GetComputers-${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()}-${date.getHours()}:${date.getMinutes()}`,
    name: 'GetComputers',
    scenario: 'GetComputers',
  },
};

export default function () {
  const url = 'http://localhost:8181/v1/computer';

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const res = http.get(url, params);
  check(res, {
    'is status 200': (resp) => resp.status === 200,
  });
}