import http from 'k6/http';
import { check } from 'k6';

const months = ["January", "February", "March", "April", "May", "June", "July",
"August", "September", "October", "November", "December"];

const date = new Date();

export let options = {
  tags: {
    testid: `PostComputers-${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()}-${date.getHours()}:${date.getMinutes()}`,
    name: 'PostComputers',
    scenario: 'PostComputers',
  },
};

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

export default function () {
  const url = 'http://localhost:8181/v1/computer';

  // send post request with custom header and payload
  for (let id = 1; id <= 10000; id++) {
    const payload = JSON.stringify({
      macAddress: `00:5e:00:53:${id}`,
      Name: `Computer-${id}`,
      ipAddress: "192.168.10.20",
      employeeAbbreviation: makeid(3),
      description: "work computer"
    });

    const res = http.post(url, payload, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    check(res, {
      'is status 200': (resp) => resp.status === 200,
    });
  }
}