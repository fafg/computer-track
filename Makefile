.EXPORT_ALL_VARIABLES:

LINT_VERSION=1.41.1
LINT_IMAGE=golangci/golangci-lint:v${LINT_VERSION}-alpine
LINT_FLAGS=--timeout=10m0s

OS="$(shell go env var GOOS | xargs)"

path :=$(if $(path), $(path), "./")
 
.PHONY: help
help: ## - Show this help message
	@printf "\033[32m\xE2\x9c\x93 usage: make [target]\n\n\033[0m"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: up
up: ## - start docker compose
	@ cd _docker && docker-compose -f docker-compose.yml up

.PHONY: build-common
build-common: ## - execute build common tasks clean and mod tidy
	@ go version
	@ go clean
	@ go mod tidy && go mod download
	@ go mod verify

.PHONY: build
build: build-common ## - build a debug binary to the current platform (windows, linux or darwin(mac))
	@ echo cleaning...
	@ rm -f _bin/debug/$(OS)/computer-track
	@ echo building...
	@ go build -tags debug -o "_bin/debug/$(OS)/computer-track" cmd/api/api-server.go
	@ ls -lah _bin/debug/$(OS)/computer-track

.PHONY: build-static-release
build-static-release: build-common ## - build a static release linux elf(binary)
	@ echo cleaning...
	@ rm -f _bin/release/$(OS)/computer-track
	@ echo building...
	@ CGO_ENABLED=0 go build -ldflags='-w -s -extldflags "-static"' -a -o "_bin/release/$(OS)/computer-track" cmd/api/api-server.go
	@ ls -lah _bin/release/$(OS)/computer-track

.PHONY: build-linux-release
build-linux-release: build-common ## - build a static release linux elf(binary)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags='-w -s -extldflags "-static"' -a -o "_bin/release/linux/computer-track" cmd/api/api-server.go;\

.PHONY: test
test: build-common ## - execute go test command
	@ go test -v -cover ./...
#	go fmt $(go list ./... | grep -v /vendor/)
#	go vet $(go list ./... | grep -v /vendor/)
#	go test -race $(go list ./... | grep -v /vendor/)

.PHONY: scan
scan: ## - execute static code analysis
	@ gosec -fmt=sarif -out=computer-track.sarif -exclude-dir=test -exclude-dir=bin -severity=medium ./... | 2>&1
	@ echo ""
	@ cat $(path)/computer-track.sarif
#	@ cat $(path)/echo.sarif | jq -r '(["LEVEL","LINE","FILE","RULE-ID","MESSAGE"] | (., map(length*"-"))), (.runs[].results[] | [.level, .locations[].physicalLocation.region.startLine, .locations[].physicalLocation.artifactLocation.uri, .ruleId, .message.text] ) | @tsv' | column -t

.PHONY: ci-lint
ci-lint: ## - runs golangci-lint
#	@ golangci-lint run --enable-all -D forbidigo -D gochecknoglobals -D gofumpt -D gofmt -D nlreturn
	@ golangci-lint run -v ${LINT_FLAGS}

.PHONY: ci-lint-docker
ci-lint-docker: ## - runs golangci-lint with docker container
	@ docker run --rm -v "$(shell pwd)":/app -w /app ${LINT_IMAGE} golangci-lint run ${LINT_FLAGS}

.PHONY: docker-build
docker-build: ## - build docker image
	@ docker build -f _docker/Dockerfile -t $(ecr)/computer-track:$(version) .

.PHONY: docker-push
docker-push:
	@ docker push $(ecr)/computer-track:$(version)

.PHONY: docker-scan
docker-scan: ## - Scan for known vulnerabilities
	@ printf "\033[32m\xE2\x9c\x93 Scan for known vulnerabilities the smallest and secured golang docker image based on scratch\n\033[0m"
	@ $(SHELL) _scripts/docker-scan.sh $(version)

.PHONY: sonar-scan
sonar-scan: ## - start sonar qube locally with docker (you will need docker installed in your machine)
	@ # login with user: admin pwd: echo
	@ $(SHELL) _scripts/sonar-start.sh

.PHONY: sonar-stop
sonar-stop: ## - stop sonar qube docker container
	@ docker stop sonarqube

local-test:
#	@ k6 run -o experimental-prometheus-rw --vus 10 --duration 4m tests/load-test/load-PostComputers-test.js
	@ k6 run -o experimental-prometheus-rw --vus 10 --duration 4m tests/load-test/load-GetComputers-test.js

run: build-static-release
	@ DB_CONN_STRING="computer_track_user:1234@tcp(localhost:3600)/computer-track-db" \
	NOTIFICATION_ENDPOINT="http://localhost:8080/api/notify" \
	./_bin/release/$(OS)/computer-track