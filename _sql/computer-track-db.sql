CREATE DATABASE IF NOT EXISTS `computer-track-db`;

USE `computer-track-db`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `computers`
--

DROP TABLE IF EXISTS `computers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
-- Some fields could use a different type, in order to save space.
-- ip_address could be varbinary(4) for example.
-- mac_address could be varbinary(6) for example.
CREATE TABLE `computers` (
     `mac_address` varchar(17) NOT NULL,
     `computer_name` varchar(255) NOT NULL,
     `ip_address` varchar(45) NOT NULL,
     `employee_abbreviation` varchar(3) DEFAULT NULL,
     `description` varchar(255) DEFAULT NULL,

     PRIMARY KEY (`mac_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP USER IF EXISTS 'computer_track_user'@'%';
CREATE USER IF NOT EXISTS 'computer_track_user'@'%' IDENTIFIED BY '1234';
GRANT SELECT, INSERT, UPDATE, DELETE ON `computer-track-db`.`computers` TO 'computer_track_user'@'%';

-- DB_CONN_STRING = 'computer_track_user:1234@tcp(localhost:3306)/computer-track-db'
